import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Shuffle {
	/**
	 * Lam Tran Illinois Institute of technology ltran12@hawk.iit.edu
	 */

	public static void main(String[] args) {

		int c = 0;
		String output = "";

		try {
			File shuffle = new File("messaging.in");
			Scanner scan = new Scanner(shuffle);
			String totalSong = null, permu = null;
			while (scan.hasNextLine()) {

				StringTokenizer token = new StringTokenizer(scan.nextLine(),
						",");
				if (token.countTokens() != 2)
					continue;

				totalSong = token.nextToken();
				permu = token.nextToken().toUpperCase();
				output += "Case #" + c++ + ":" + checkValid(totalSong, permu)
						+ "\n";

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// System.out.println(output);
		try {
			FileWriter wr = new FileWriter(new File("../data/shuffle.out"));
			wr.write(output);
			wr.flush();
			wr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int checkValid(String num, String song) {

		int numSong = 0;

		try {
			numSong = Integer.parseInt(num);
		} catch (NumberFormatException ex) {
			return -1;
		}

		if (numSong > 26)
			return -1;
		int[] array = null;

		for (int j = 0; j < numSong; j++) {
			array = new int[26];

			for (int k = 0; k < j; k++) {
				char watse = song.charAt(k);
				if (++array[watse - 65] == 2)
					return -1;
			}

			array = new int[26];
			int count = 0;

			String song1 = song.substring(j, song.length());
			for (int i = 0; i < song1.length(); i++) {
				char songName = song1.charAt(i);

				try {
					if (++array[songName - 65] == 2) {
						if (count == numSong) {
							count = 1;
							array = new int[26];
							array[songName - 65] = 1;
						} else
							i = song1.length() + 1;
					} else {
						count++;
						if (count > numSong)
							return -1;
					}
					if (i == song1.length() - 1)
						return j;
				} catch (ArrayIndexOutOfBoundsException ex) {
					System.out.println("There are more songs than you said");
				}
			}

		}
		return -1;
	}
}
